<?php
session_start();

$dirName = "attestations";

$dir = dir($dirName);
$file_list = array();
$result = null;

if(empty($_SESSION['login']) and $_SESSION['level'] != 'user'){
    header("Location: index.php");
    exit();
}

while($entry = $dir->read()) {
    if($entry != '..' and $entry != '.')
        //Support hash sha256
        array_push($file_list, strtoupper(hash_file("sha256", $dirName."/".$entry)));
}
$dir->close();

?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Connexion</title>
    <!-- ** Import JS and CSS bootstrap ** -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/bootstrap-grid.css">
    <link rel="stylesheet" href="css/bootstrap-reboot.css">
    <link rel="stylesheet" href="css/form.css">
    <script src="js/jquery-3.5.0.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/bootstrap.bundle.js"></script>
</head>
<body>
<!-- Main connexion container -->
<div class="container">

    <header>
        <div class="disconnect_container">
            <div class="profil_part">
                <p>You are : <?php echo $_SESSION['login'];?></p>
                <button class="btn btn-lg btn-primary btn-block text-uppercase disconnect" onclick="window.location.href='deconnect.php'">disconnect</button>
            </div>
        </div>
    </header>

    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin my-5">
                <div class="card-body">
                    <!-- Title -->
                    <h5 class="card-title text-center">Attestation finder form</h5>
                    <!-- Form for connexion -->
                    <form class="form-signin" action="user_index.php" method="post">
                        <!-- file hash -->
                        <div class="form-label-group">
                            <label for="hashFile">Hash of file :</label>
                            <input type="text" id="hashFile" name="hashFile" placeholder="write hash"/>
                        </div>
                        <!-- button to submit form -->
                        <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Find attestation</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-10 mx-auto">
            <div class="card card-signin my-5">
                <div class="card-body">
                    <!-- Title -->
                    <h5 class="card-title text-center">Result</h5>

                    <p>Total items : <?php echo sizeof($file_list) ?></p>

                    <div class="result">
                        <?php
                            if(isset($_POST['hashFile'])){
                                echo "<p>Query : ".$_POST['hashFile']."</p>";

                                if(in_array($_POST['hashFile'], $file_list, TRUE)){
                                    echo "<p>File link : <a href='show_pdf.php?name=".$_POST['hashFile']."'>file</a></p>";
                                    echo "<iframe src=\"".$dirName."/".$_POST['hashFile'].".pdf\" width=\"100%\" style=\"height:500px\"></iframe>";
                                }else{
                                    echo "<p>No results found !</p>";
                                }
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
