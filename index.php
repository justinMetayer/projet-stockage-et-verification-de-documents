<?php
    session_start();
    if(isset($_POST['login']) and isset($_POST['password'])){
        $login = $_POST['login'];
        $password = $_POST['password'];

        //Authentification primaire
        if($login == 'Columbo' and $password == '123'){
            $_SESSION['login'] = $login;
            $_SESSION['level'] = 'user';
            echo "redirect";
            header("Location: user_index.php");
            exit();
        }
    }
?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Connexion</title>
    <!-- ** Import JS and CSS bootstrap ** -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/bootstrap-grid.css">
    <link rel="stylesheet" href="css/bootstrap-reboot.css">
    <link rel="stylesheet" href="css/form.css">
    <script src="js/jquery-3.5.0.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/bootstrap.bundle.js"></script>
</head>
<body>
<!-- Main connexion container -->
<div class="container">
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin my-5">
                <div class="card-body">
                    <!-- Title -->
                    <h5 class="card-title text-center">Connexion form</h5>
                    <!-- Form for connexion -->
                    <form class="form-signin" method="post" action="index.php">
                        <!-- login input and label -->
                        <div class="form-label-group">
                            <label for="login">Login :</label>
                            <input type="text", id="login" name="login" placeholder="write login"/>
                        </div>
                        <!-- password input and label -->
                        <div class="form-label-group">
                            <label for="password">Password :</label>
                            <input type="password" id="password" name="password" placeholder="write password"/>
                        </div>
                        <!-- button to submit form -->
                        <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Connexion</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>